package com.example.gabriellamurman.jsonparse;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ItemAdapter extends ArrayAdapter<Users> {

    private Context context;
    private ArrayList<Users> items;

    public ItemAdapter(Context context, int resource, ArrayList<Users> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if (rowView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item, parent, false);
        }

        // Set data into the view.
        TextView name = (TextView) rowView.findViewById(R.id.name);
        TextView username = (TextView) rowView.findViewById(R.id.username);
        TextView email = (TextView) rowView.findViewById(R.id.email);

        //get Item
        Users item = this.items.get(position);


        name.setText(item.getName());
        username.setText(item.getUsername());
        email.setText(item.getEmail());



        //http://stackoverflow.com/questions/2471935/how-to-load-an-imageview-by-url-in-android
        //Para obtener una img de internet y pintarla

        return rowView;
    }
}