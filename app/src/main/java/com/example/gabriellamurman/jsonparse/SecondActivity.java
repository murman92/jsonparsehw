package com.example.gabriellamurman.jsonparse;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView editText_name,editText_username,editText_email, editText_street, editText_suite, editText_city,
            editText_zipcode, editText_lat, editText_lng;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //get Users
        Users c = (Users)getIntent().getSerializableExtra("contact");

        //FindviewByIds
        editText_name = (TextView) findViewById(R.id.editText_name);
        editText_username = (TextView) findViewById(R.id.editText_username);
        editText_email = (TextView) findViewById(R.id.editText_email);
        editText_street = (TextView) findViewById(R.id.editText_street);
        editText_suite = (TextView) findViewById(R.id.editText_suite);
        editText_city = (TextView) findViewById(R.id.editText_city);
        editText_zipcode = (TextView) findViewById(R.id.editText_zipcode);
        editText_lat = (TextView) findViewById(R.id.editText_lat);
        editText_lng = (TextView) findViewById(R.id.editText_lng);


        btn_back = (Button) findViewById(R.id.btn_back);

        //SET TEXT
        editText_name.setText(c.getName());
        editText_username.setText(c.getUsername());
        editText_email.setText(c.getEmail());
        editText_street.setText(c.getStreet());
        editText_suite.setText(c.getSuite());
        editText_city.setText(c.getCity());
        editText_zipcode.setText(c.getZipcode());
        editText_lat.setText(c.getLat());
        editText_lng.setText(c.getLng());


        //back
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}