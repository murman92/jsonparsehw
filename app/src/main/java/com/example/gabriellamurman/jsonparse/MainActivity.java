package com.example.gabriellamurman.jsonparse;

        import android.content.Context;
        import android.content.Intent;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.net.wifi.WifiManager;
        import android.os.AsyncTask;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ListAdapter;
        import android.widget.ListView;
        import android.widget.Toast;
        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedReader;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    public ListView list;
    public ArrayList<Users> usersList = new ArrayList<>();
    public String jsonStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("JSON HOMEWORK :)");


        new GetUsers().execute("http://jsonplaceholder.typicode.com/users");

    }

    private boolean isConnected () {
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        boolean isMobileConn = networkInfo.isConnected();

        Log.d(TAG, "Wifi connected: " + isWifiConn);
        Log.d(TAG, "Mobile connected: " + isMobileConn);

        if (isWifiConn || isMobileConn) {
            return true;
        }else{
            return false;
        }

    }


    private class GetUsers extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this, "GetUsers onPreExecute()", Toast.LENGTH_LONG).show();

            if(!isConnected()){
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
            }


        }

        @Override
        protected String doInBackground(String... arg) {

            if(isConnected()){
                // Making a request to url and getting response
                HttpHandler sh = new HttpHandler();
                jsonStr = sh.makeServiceCall(arg[0]);
            }else{

                String filename="users.json";
                StringBuffer stringBuffer = new StringBuffer();

                try {
                    BufferedReader inputReader = new BufferedReader(new InputStreamReader(openFileInput(filename)));
                    String inputString;

                    while ((inputString = inputReader.readLine()) != null) {
                        stringBuffer.append(inputString);
                    }

                    jsonStr = stringBuffer.toString();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (ParseJson())
                return "AsyncTask finished";
            else
                return "Error";
        }


        @Override
        protected void onPostExecute(String retString) {

            Toast.makeText(MainActivity.this, retString, Toast.LENGTH_LONG).show();

            //findViewByIds
            list = (ListView) findViewById(R.id.list);

            //List Adapter
            ListAdapter adapter = new ItemAdapter(getApplicationContext(),android.R.layout.simple_list_item_1, usersList);
            list.setAdapter(adapter);

            WriteJsonInLocalStorage();


            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, final int position, long arg) {

                    //Toast.makeText(getApplicationContext(),usersList.get(position).getEmail(),Toast.LENGTH_LONG).show();

                    Intent i = new Intent(getApplicationContext(),SecondActivity.class);
                    i.putExtra("contact", usersList.get(position));
                    startActivity(i);
                }
            });


        }


        public void WriteJsonInLocalStorage() {

            String filename="users.json";
            String data= jsonStr;

            FileOutputStream fos;

            try {
                fos = openFileOutput(filename, Context.MODE_PRIVATE);
                fos.write(data.getBytes());
                fos.close();

                Toast.makeText(getApplicationContext(), filename + " saved", Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                Log.d("Main_activity",e.getMessage());
            }

        }
//////

        public Boolean ParseJson () {
            try {
                JSONArray jsonObj = new JSONArray(jsonStr);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject c = jsonObj.getJSONObject(i);
                    String id = c.getString("id");
                    String name = c.getString("name");
                    String username = c.getString("username");
                    String email = c.getString("email");

                    JSONObject address = c.getJSONObject("address");
                    String street = address.getString("street");
                    String suite = address.getString("suite");
                    String city = address.getString("city");
                    String zipcode = address.getString("zipcode");

                    JSONObject geo = address.getJSONObject("geo");
                    String lat = geo.getString("lat");
                    String lng = geo.getString("lng");

                    usersList.add(new Users(id, name, username, email, street, suite, city, zipcode, lat, lng));
                }

            } catch (final JSONException e) {
                Log.e(TAG, "Impossible to download the json file.");
                Toast.makeText(getApplicationContext(),"Impossible to download the json.",Toast.LENGTH_LONG).show();
                return false;
            }

            return true;
        }
    }


}